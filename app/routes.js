const { exchangeRates } = require('../src/util.js');
const express = require("express");
const router = express.Router();

	router.get('/rates', (req, res) => {
		return res.status(200).send(exchangeRates);
	})

	router.post('/currency', (req, res) => {
		if(!req.body.hasOwnProperty("name")){
			return res.status(400).send({
				error: 'Error - missing required parameter NAME'
			})
		}

		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				error: 'Error - required parameter NAME should be STRING'
			})
		}

		if(req.body.name == " " || req.body.name == null){
			return res.status(400).send({
				error: 'Error - required parameter NAME should not be EMPTY'
			})
		}

		if(!req.body.hasOwnProperty("ex")){
			return res.status(400).send({
				error: 'Error - missing required parameter EX'
			})
		}

		if(typeof req.body.ex !== 'object'){
			return res.status(400).send({
				error: 'Error - required parameter EX should be OBJECT'
			})
		}

		if(Object.keys(req.body.ex).length === 0){
			return res.status(400).send({
				error: 'Error - required parameter EX should not be EMPTY'
			})
		}

		if(!req.body.hasOwnProperty("alias")){
			return res.status(400).send({
				error: 'Error - missing required parameter ALIAS'
			})
		}

		if(typeof req.body.alias !== 'string'){
			return res.status(400).send({
				error: 'Error - required parameter ALIAS should be STRING'
			})
		}

		if(req.body.alias == " " || req.body.alias == null){
			return res.status(400).send({
				error: 'Error - required parameter ALIAS should not be EMPTY'
			})
		}

		let duplicateAlias = exchangeRates.find((d) => {
		    return d.alias === req.body.alias
		});

		if(duplicateAlias){
    		return res.status(400).send({
    			error : 'Error - required parameter ALIAS has a duplicate'
    		})
    	}

    	else {    		
    		return res.status(200).send({
    			success : 'No error found'
    		});
    	}    	
	})

module.exports = router;
