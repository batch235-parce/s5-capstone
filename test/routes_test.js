const chai = require('chai');
const{ assert }= require('chai');
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', (done) => {
		chai.request('http://localhost:5001').get('/forex/getRates')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/forex/rates')
		.end((err, res) => {
			assert.equal(res.status,200)
			done();	
		})		
	})
	

	it('test_api_post_currency_returns_200_if_complete_input_given', () => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)
			
		})
	})

})




// CAPSTONE
describe("Currency Test Suite", () => {
	// 1
	it('Test API currency is running', (done) => {
		chai.request('http://localhost:5001').get('/forex/currency')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})

	// 2
	it('Test currency post returns 400 if name is missing', (done) => {
		chai.request('http://localhost:5001').post('/forex/currency')
		.type("json")
		.send({
			'alias': 'euro',
	      	'ex': {
	        	'usd': 0.96,
	        	'peso': 56.80,
	        	'won': 1378,
	        	'yen': 139.21
	        }
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done();
		})
	})

	// 3
	it('Test currency post returns 400 if name is not a string', (done) => {
		chai.request('http://localhost:5001').post('/forex/currency')
		.type("json")
		.send({
			'alias': 'euro',
			'name' : 24/365,
	      	'ex': {
	        	'usd': 0.96,
	        	'peso': 56.80,
	        	'won': 1378,
	        	'yen': 139.21
	        }
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done();
		})
	})

	// 4
	it('Test currency post returns 400 if name is an empty string', (done) => {
		chai.request('http://localhost:5001').post('/forex/currency')
		.type("json")
		.send({
			'alias': 'euro',
			'name' : " ",
	      	'ex': {
	        	'usd': 0.96,
	        	'peso': 56.80,
	        	'won': 1378,
	        	'yen': 139.21
	        }
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done();
		})
	})

	// 5
	it('Test currency post returns 400 if ex is missing', (done) => {
		chai.request('http://localhost:5001').post('/forex/currency')
		.type("json")
		.send({
			'alias': 'euro',
			'name' : "Currency of European Union"
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done();
		})
	})

	// 6
	it('Test currency post returns 400 if ex is not an object', (done) => {
		chai.request('http://localhost:5001').post('/forex/currency')
		.type("json")
		.send({
			'alias': 'euro',
			'name' : "Currency of European Union",
			'ex' : "usd : 0.96, peso : 56.80 won : 1378, yen : 139.2"
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done();
		})
	})

	// 7
	it('Test currency post returns 400 if ex is an empty object', (done) => {
		chai.request('http://localhost:5001').post('/forex/currency')
		.type("json")
		.send({
			'alias': 'euro',
			'name' : "Currency of European Union",
			'ex': { }
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done();
		})
	})

	// 8
	it('Test currency post returns 400 if alias is missing', (done) => {
		chai.request('http://localhost:5001').post('/forex/currency')
		.type("json")
		.send({
			'name' : "Currency of European Union",
			'ex': {
	        	'usd': 0.96,
	        	'peso': 56.80,
	        	'won': 1378,
	        	'yen': 139.21
	        }
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done();
		})
	})

	// 9
	it('Test currency post returns 400 if alias is not a string', (done) => {
		chai.request('http://localhost:5001').post('/forex/currency')
		.type("json")
		.send({
			'alias': 24/365,
			'name' : "Currency of European Union",
			'ex': {
	        	'usd': 0.96,
	        	'peso': 56.80,
	        	'won': 1378,
	        	'yen': 139.21
	        }
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done();
		})
	})

	// 10
	it('Test currency post returns 400 if alias is an empty string', (done) => {
		chai.request('http://localhost:5001').post('/forex/currency')
		.type("json")
		.send({
			'alias': " ",
			'name' : "Currency of European Union",
			'ex': {
	        	'usd': 0.96,
	        	'peso': 56.80,
	        	'won': 1378,
	        	'yen': 139.21
	        }
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done();
		})
	})

	// 11
	it('Test currency post returns 400 if alias has duplicates', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
        .type('json')
        .send({
            'alias': "usd",
            'name' : "Currency of European Union",
			'ex': {
	        	'usd': 0.96,
	        	'peso': 56.80,
	        	'won': 1378,
	        	'yen': 139.21
	        }
        })
        .end((err, res) => {
            assert.equal(res.status, 400);
            done();
        })
	})

	// 12
	it('Test currency post returns 200 if fields are complete and no duplicates', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
        .type('json')
        .send({
            'alias': "euro",
            'name' : "Currency of European Union",
			'ex': {
	        	'usd': 0.96,
	        	'peso': 56.80,
	        	'won': 1378,
	        	'yen': 139.21
	        }
        })
        .end((err, res) => {
            assert.equal(res.status, 200);
            done();
        })
	})



})
